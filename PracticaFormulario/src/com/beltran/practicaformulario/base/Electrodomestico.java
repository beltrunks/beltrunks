package com.beltran.practicaformulario.base;

public class Electrodomestico {
    String marca;
    String modelo;
    String precio;


    // Creo la clase Electrodomestico con sus dos atributos marca y modelo y creo también un constructor con los dos atributos y los setter y getter para estos

    public Electrodomestico(String marca, String modelo) {
        this.marca = marca;
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    // Creo tambien el toString


    @Override
    public String toString() {
        return "Electrodomestico{" +
                "marca='" + marca + '\'' +
                ", modelo='" + modelo + '\'' +
                '}';
    }
}
