
import com.beltran.practicaformulario.base.Electrodomestico;
import org.w3c.dom.*;
import org.xml.sax.SAXException;


import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

public class Vista {
    //Elementos añadidos automaticamente a partir del formulario grafico
    private JFrame frame;
    private JPanel panel1;
    private JTextField marcaTxt;
    private JTextField modeloTxt;
    private JComboBox comboBox;
    private JButton altaElectrodomesticoBtn;
    private JButton mostrarElectrodomesticoBtn;
    private JLabel lblElectrodomestico;
    private JButton mostrarTodosBtn;
    private JButton contarTotalBtn;

    //Elementos añadidos por mi
    private LinkedList<Electrodomestico> lista;
    private DefaultComboBoxModel dcbm;

    public Vista() {
        //Instanciamos el frame y le añadimos nuestra ventana panel1, poenmos para que se cierre al pulsar el boton cerrar,
        // para que se adapte al cambiarle el tamaño y lo ponemos para que sea visible
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);


        crearMenu();
        frame.setLocationRelativeTo(null);
        lista = new LinkedList<>();
        dcbm = new DefaultComboBoxModel();
        comboBox.setModel(dcbm);

        altaElectrodomesticoBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //dar de alta electrodomestico
                altaElectrodomestico(marcaTxt.getText(),modeloTxt.getText());
                //listar electrodomesticos de la lista del combo
                refrescarComboBox();
            }
        });
        mostrarElectrodomesticoBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //muestro el electrodomestico seleccionado
                Electrodomestico seleccionado = (Electrodomestico) dcbm.getSelectedItem();
                lblElectrodomestico.setText(seleccionado.toString());
            }
        });
        //Mostrar todos los elementos de la lista
        mostrarTodosBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                lblElectrodomestico.setText(lista.toString());
            }
        });
        //Contar los elementos de la lista y mostrar el numero total
        contarTotalBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                lblElectrodomestico.setText(Integer.toString(lista.size()));
            }
        });
    }

    private void refrescarComboBox(){
        dcbm.removeAllElements();;
        for (Electrodomestico electrodomestico : lista) {
            dcbm.addElement(electrodomestico);
        }
    }

    //Desde el main solo llamamos a vista ya que en su constructor esta el codigo a ejecutar
    public static void main(String[] args) {
        Vista vista = new Vista();
    }

    //Metodo para añadir un electrodomestico a nuestra lista
    private void altaElectrodomestico(String marca, String modelo) {
        lista.add(new Electrodomestico(marca, modelo));
    }

    private void crearMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        JMenuItem itemExportarXML = new JMenuItem("Exportar XML");
        JMenuItem itemImportarXML = new JMenuItem("Importar XML");

        itemExportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcionSeleccionada = selectorArchivo.showSaveDialog(null);
                if (opcionSeleccionada == JFileChooser.APPROVE_OPTION) {
                    File fichero = selectorArchivo.getSelectedFile();
                    importarXML(fichero);
                    refrescarComboBox();
                }
            }
        });

        itemImportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcion = selectorArchivo.showOpenDialog(null);
                if (opcion == JFileChooser.APPROVE_OPTION) {
                    File fichero = selectorArchivo.getSelectedFile();
                    importarXML(fichero);
                    refrescarComboBox();
                }
            }
        });

        menu.add(itemExportarXML);
        menu.add(itemImportarXML);

        barra.add(menu);
        frame.setJMenuBar(barra);
    }

    private void importarXML(File fichero) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document documento = builder.parse(fichero);

            //Recorro cada uno de los nodos electrodomestico para obtener sus campos
            NodeList electrodomesticos = documento.getElementsByTagName("electrodomestico");
            for (int i = 0; i < electrodomesticos.getLength(); i++) {
                Node coche = electrodomesticos.item(i);
                Element elemento = (Element) coche;

                //Obtengo los campos marca y modelo
                String marca = elemento.getElementsByTagName("marca").item(0).getChildNodes().item(0).getNodeValue();
                String modelo = elemento.getElementsByTagName("modelo").item(0).getChildNodes().item(0).getNodeValue();

                altaElectrodomestico(marca, modelo);
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
    }

    private void exportarXML(File fichero) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;

        try {
            builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();

            //Creo documento que representa arbol XML
            Document documento = dom.createDocument(null, "xml", null);

            //Creo el nodo raiz (electrodomesticos) y lo añado al documento
            Element raiz = documento.createElement("electrodomesticos");
            documento.getDocumentElement().appendChild(raiz);

            Element nodoElectrodomestico;
            Element nodoDatos;
            Text dato;

            //Por cada electrodomestico de la lista, creo un nodo electrodomestico
            for (Electrodomestico electrodomestico : lista) {

                //Creo un nodo electrodomestico y lo añado al nodo raiz (electrodomesticos)
                nodoElectrodomestico = documento.createElement("electrodomestico");
                raiz.appendChild(nodoElectrodomestico);

                //A cada nodo electrodomestico le añado los nodos marca y modelo
                nodoDatos = documento.createElement("marca");
                nodoElectrodomestico.appendChild(nodoDatos);

                //A cada nodo de datos le añado el dato
                dato = documento.createTextNode(electrodomestico.getMarca());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("modelo");
                nodoElectrodomestico.appendChild(nodoDatos);

                dato = documento.createTextNode(electrodomestico.getModelo());
                nodoDatos.appendChild(dato);
            }

            //Transformo el documento anterior en un fichero de texto plano
            Source src = new DOMSource(documento);
            Result result = new StreamResult(fichero);

            Transformer transformer = null;
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(src, result);

        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

}
